package agenda;

import br.com.senac.model.Contato;
import br.com.senac.views.*;
import java.util.List;
import java.util.ArrayList;

public class AgendaContatos {

    private static List<Contato> listaDeContatos = new ArrayList<>();

    public static void adicionar(Contato contato) {
        listaDeContatos.add(contato);
    }

    public static void remover(Contato contato) {
        listaDeContatos.remove(contato);
    }

    public static void remover(int indice) {
        listaDeContatos.remove(indice);
    }

    public static int getQuantContatos() {
        return listaDeContatos.size();
    }

    private static JFramePrincipal f;

    public static void main(String[] args) {
        f = new JFramePrincipal();
    }

    public static List<Contato> getListaDeContatos() {
        return listaDeContatos;
    }
    
    

}
